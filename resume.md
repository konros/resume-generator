---
Title: Resume
---



# Dan Williams
## Infrastructure Platform Engineer

Windows and Linux systems engineer with over 13 years of experience in infrastructure consulting across multiple platform technologies. With a proven ability to work and communicate at many organizational levels with both internal staff and external customers.

### Experience

#### Orion Business Innovation
**Senior Platform Infrastructure Engineer**
*Current - May 2015*

- Wrote Ansible roles covering a diverse environment of over 5000 nodes running different versions of Debian/CentOS. Migrating various components out of dpkg/Jenkins and into Ansible in the progress. 
- Modernized infrastructure of legacy LAPP application from AWS IAAS VMs to containers on Azure Kubernetes Service.
- Implemented health monitoring and uptime dashboard for a B2B sales app (IIS, Apache, ElasticSearch, ProgressDB, MSSQL) using Grafana, Prometheus, Graylog, and Statuspage.io.
- Advocated for moving secrets to HashiCorp Vault and deployed a POC streamlining private key rotation and managing access through Github Auth.
- Developed tooling and automation to streamline end-user and business processes.

#### Business Machine Technologies
**System Administrator**
*March 2015 - August 2012*

- Migrated client from Citrix environment to Microsoft RDS environment which reduced software licensing costs by 30%.
- Upgraded over 30 small and medium sized business from local Exchange Server 2003 - 2013 to Office 365.
- Designed, implemented, and supported a Disaster Recovery and Backup Site with vCenter Site Recovery and StorageCraft’s ShadowProtect.
- Saved a client a substantial amount of data center real estate by converting 20+ physical servers to virtual (P2V).
- Provided Tier 3 escalation & support for help desk technicians and jr. system admins.

#### Reckitt Benckiser
**Systems Analyst**
*August  2012 - December 2011*

- Administered and maintained an IT infrastructure for the corporate headquarters with over 100 Windows servers, 600 workstations, and technical business equipment, including networked devices, document scanners, printers, and a Cisco phone system.
- Automated maintenance tasks with a combination of Powershell, Visual Basic, and Bash scripts.
- Established robust and varied documentation, inventory, monitoring and management systems for all IT operations.

#### EXL Service
**Junior System Administrator**
*June 2009 - July 2006*

- Replaced out of warranty Cisco firewalls with Juniper  appliances and reconfigured all access rules and site-to-site VPNs.
- Performed diagnostics and debugging, including but not limited to resolving BSODs, network issues, forensic troubleshooting, malware removal, and freezing on critical servers and workstations.
- Performed end-user computer diagnostics and repair on Windows, Mac, & Linux workstations.


### Skills

#### Containers
- Kubernetes
- Docker
- LXC

#### Tooling
- Terraform
- Packer
- Ansible
- Chef
- Powershell DSC
- Jenkins
- Git

#### Infrastructure Platforms
- AWS
- Azure
- VMWare ESXI
- Hyper-V
- Citrix

#### Languages
- Ruby
- Python
- Bash
- Powershell
- HTML/CSS/JS

#### Monitoring and Logging
- Prometheus
- Datadog
- Grafana
- ELK
- Graylog
- Nagios
